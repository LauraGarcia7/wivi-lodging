import { Routes } from '@angular/router';
import { PaginaprincipalComponent } from '../../paginaprincipal/paginaprincipal.component';
import { LoginComponent } from '../../login/login.component';
import { RegistroComponent } from '../../registro/registro.component';
import { LibroComponent } from '../../libro/libro.component';


export const AdminLayoutRoutes: Routes = [
    { path: 'paginaprincipal',  component: PaginaprincipalComponent },
    { path: 'login',  component: LoginComponent },
    { path: 'registro',  component: RegistroComponent },
    { path: 'hospedaje',  component: LibroComponent },
];
